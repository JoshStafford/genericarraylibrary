import tools.ISet;

public class ArraySet<T extends Comparable<T>> implements ISet<T>{

    public T[] data;
    private int top = 0;


    @SuppressWarnings("unchecked")
    ArraySet(int size){

        data = (T[]) new Comparable[size];

    }

    public boolean isFull(){
        return top == data.length;
    }

    @Override
    public boolean isEmpty(){
        return top == 0;
    }

    @Override
    public boolean add(T element) {

        if(this.isFull()){
            return false;
        }

        data[top] = element;
        top++;
        return true;

    }

    @Override
    public boolean addAll(T[] arr) {

        if(data.length - top < arr.length){
            return false;
        }

        for(T element : arr) {

            this.add(element);

        }

        return true;

    }

    @Override
    public void clear() {

        int size = data.length;
        data = (T[]) new Object[size];

    }

    @Override
    public boolean contains(T element) {

        if(this.isEmpty()){
            return false;
        }

        boolean found = false;

        for(int i = 0; i < top; i++) {

            if(data[i].equals(element)){
                found = true;
            }

        }
        return found;

    }

    @Override
    public boolean remove(T element) {

        if(this.isEmpty()){
            return false;
        }

        int index = -1;

        for(int i = 0; i < data.length; i++) {

            if(data[i].equals(element)){
                index = i;
            }

        }

        if(index == -1){
            return true;
        }

        for(int i = index; i < data.length-1; i++) {

            T temp = data[i];
            data[i] = data[i+1];
            data[i+1] = temp;

        }

        top--;
        return true;

    }

    @Override
    public boolean removeAll(T[] arr) {

        if(this.isEmpty()){
            return false;
        }

        for(T element : arr) {
            this.remove(element);
        }

        return true;

    }

    @Override
    public int size(){
        return data.length;
    }


    public static void main(String[] args) {

        ISet<Integer> arr = new ArraySet<Integer>(10);

        arr.add(7);
        arr.add(16);
        arr.add(9);

        System.out.println(arr.contains(9));

    }

}