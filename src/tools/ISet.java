package tools;

public interface ISet<E extends Comparable<E>> {

    // Adds object to collection
    boolean add(E e);

    // Adds all objects from collection to set
    boolean addAll(E[] e);

    // Removes all object from collection
    void clear();

    // Returns true if specified object is element in collection
    boolean contains(E e);

    // Returns true if collection has no elements
    boolean isEmpty();

    // Removes specified object from collection
    boolean remove(E e);

    // Removes all objects in collection from set
    boolean removeAll(E[] e);

    // Returns number of elements in collection
    int size();

}