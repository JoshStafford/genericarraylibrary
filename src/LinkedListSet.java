import tools.ISet;

public class LinkedListSet<T extends Comparable<T>> implements ISet<T> {

    private Node<T> first;

    LinkedListSet(T first_element){

        first = new Node<T>(first_element);

    }

    private Node<T> getLast() {

        Node<T> current = this.first;

        while(current.getNext() != null) {

            current = current.getNext();

        }

        return current;

    }

    @Override
    public boolean add(T element) {

        this.getLast().addNode(element);

        return true;

    }

    @Override
    public boolean addAll(T[] arr) {

        Node<T> current = this.getLast();

        for(T item : arr) {

            current.addNode(item);
            current = current.getNext();

        }

        return true;

    }

    @Override
    public void clear(){

        first = new Node<T>();

    }

    @Override
    public boolean contains(T element) {

        Node<T> current = first;
        boolean found = false;
        
        while(current != null && found == false){

            if(current.getVal() == element){

                found = true;

            } else {

                current = current.getNext();

            }

        }

        return found;

    }

    @Override
    public boolean isEmpty() {

        return first.getVal() == null;

    }

    @Override
    public boolean remove(T element) {

        if(!this.contains(element)){
            return false;
        }

        Node<T> current = this.first;

        while(current != null) {

            if(current.getNext().getVal().equals(element)){

                current.setNext(current.getNext().getNext());
                break;

            }

        }

        return true;

    }

    @Override
    public boolean removeAll(T[] arr) {

        Node<T> current = this.first;
        boolean removed = true;

        for(T item : arr) {

            if(!this.remove(item)){
                removed = false;
            }

        }

        return removed;

    }

    @Override
    public int size(){

        int count = 1;
        Node<T> current = first;

        while(current != null){
            count++;
            current = current.getNext();
        }

        return count;

    }

    public void show() {

        Node<T> current = first;

        while(current != null){

            System.out.println(current.getVal());
            current = current.getNext();

        }

    }

    public static void main(String[] args) {

        LinkedListSet<Integer> arr = new LinkedListSet<Integer>(5);

        Integer[] _arr = {4, 3, 2, 1};

        arr.addAll(_arr);
        arr.show();

    }

}