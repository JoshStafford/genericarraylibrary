public class Node<T extends Comparable<T>> {

    private T val = null;
    private Node<T> next = null;

    Node(){}

    Node(T value){

        val = value;

    }

    public T getVal() {

        return val;

    }

    public void setVal(T value){

        val = value;

    }

    public Node<T> getNext(){

        return next;

    }

    public void addNode(T value) {

        next = new Node<T>(value);

    }

    public void setNext(Node<T> new_next) {

        next = new_next;

    }

}